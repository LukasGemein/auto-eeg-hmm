import logging

from feature_generation import feature_generator
from preprocessing import preprocessor
from training import hmm_trainer
from cleaning import cleaner
from utils import io


class Pipeline(object):
    """ The pipeline object is a container for all the steps for the machine learning pipeline, i.e. preprocessor,
    trainer, predictor, preprocessor. Moreover, there is a stats object that gathers information about a pipeline run.
    """

# ______________________________________________________________________________________________________________________
    def run(self, cmd_args):
        if 'all' in cmd_args.elecs:
            cmd_args.elecs = cleaner.ELECTRODES
        if 'all' in cmd_args.domain:
            cmd_args.domain = feature_generator.DOMAINS
        else:
            cmd_args.domain = sorted(cmd_args.domain)

        # ------------------------------------------- PREPROCESSING ---------------------------------------------------#
        logging.info('\t\tPreprocessing ...')
        if not cmd_args.no_pre:
            self.preprocessor = preprocessor.Preprocessor()
            cmd_args.input = self.preprocessor.preprocess(cmd_args)

        # if preprocessing is skipped, hdf5 files are read and information from them is extracted to update cmd args
        # mostly because of result tracking and logging reasons
        if cmd_args.no_pre or (not cmd_args.no_pre and not cmd_args.pre_only):
            # inputs = io.read_all_file_names(cmd_args.output, '.hdf5', key='natural')
            cmd_args = io.update_cmd_args(cmd_args)
            keys = cmd_args.__dict__
            logging.info('\t\tUpdated the cmd arguments to ...\n\t\t\t\t' +
                         '\n\t\t\t\t'.join(map(lambda x: ': '.join([x, str(keys[x])]), keys)))

# -------------------------------------------- TRAINING / CV / AUTOSKLEARN --------------------------------------------#
            logging.info('\t\tCV ...')
            self.trainer = hmm_trainer.HMMTrainer(n_proc=cmd_args.n_proc)
            self.trainer.train(cmd_args.input)
            results = self.trainer.get_results()
            for result_id, result in enumerate(results):
                logging.info("Fold {}".format(result_id))
                logging.info(result)

# ______________________________________________________________________________________________________________________
    def __init__(self):
        self.preprocessor = None
        self.trainer = None
