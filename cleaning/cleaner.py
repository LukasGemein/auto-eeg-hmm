import numpy as np
import mne

ELECTRODES = np.asarray(sorted([
    ' FP1', ' FP2',
    ' F3', ' F4',
    ' C3', ' C4',
    ' P3', ' P4',
    ' O1', ' O2',
    ' F7', ' F8',
    ' T3', ' T4',
    ' T5', ' T6',
    ' A1', ' A2',
    ' FZ',
    ' CZ',
    ' PZ',
]))

# TODO: speed up loading of data by only loading relevant electrodes


class DataCleaner(object):
    """
    """

# ______________________________________________________________________________________________________________________
    @staticmethod
    def get_supported_electrodes():
        return ELECTRODES

# ______________________________________________________________________________________________________________________
    def rename_electrodes(self, rec):
        """ Check if the recording has all the electrodes that should be processed """
        used_electrodes = []
        for electrode_to_use in self.electrodes_to_use:
            for signal_name in rec.signal_names:
                if electrode_to_use in signal_name:
                    if electrode_to_use == ' C3' or electrode_to_use == ' C4':
                        if 'DC' in signal_name or 'C3P' in signal_name or 'C4P' in signal_name:
                            continue

                    # replace the name given in the file with the id
                    used_electrodes.append(rec.signal_names.index(signal_name))

        rec.signal_names = used_electrodes
        return rec

# ______________________________________________________________________________________________________________________
    def take_subsets_of_elecs(self):
        """ takes a subset of electrodes as specified by cmd argument"""
        if 'all' in self.elecs:
            return ELECTRODES
        else:
            # transform to upper case in case the user accidentally inputs lowercase
            self.elecs = [elec.upper() for elec in self.elecs]

            new_electrodes_to_use_ids = []
            for elec in self.elecs:
                for electrode_id, electrode in enumerate(ELECTRODES):
                    if elec in electrode:
                        new_electrodes_to_use_ids.append(electrode_id)

            new_electrodes_to_use_ids = np.asarray(sorted(new_electrodes_to_use_ids))
            electrodes_to_use = sorted(np.unique(ELECTRODES[new_electrodes_to_use_ids]))
            return electrodes_to_use

# ______________________________________________________________________________________________________________________
    def band_filter(self, rec):
        """ use mne filter to filter the frequency bands for synchronicity features """
        n_bands = len(self.bands)-1
        (n_signals, n_samples) = rec.signals.shape
        band_filtered_signals = np.zeros((n_bands, n_signals, n_samples))

        for subband_id in range(n_bands):
            band_signal = mne.filter.filter_data(
                data=rec.signals,
                sfreq=rec.sampling_freq,
                l_freq=self.bands[subband_id],
                h_freq=self.bands[subband_id+1],
                verbose='error',
                n_jobs=self.n_jobs
            )
            band_filtered_signals[subband_id] = band_signal
        rec.signals_band_filtered = band_filtered_signals

        return rec

# ______________________________________________________________________________________________________________________
    def filter_power_line_frequency(self, rec):
        """ Remove the power line frequency from the recordings """
        rec.signals = mne.filter.notch_filter(
            rec.signals,
            rec.sampling_freq,
            np.arange(self.power_line_freq, rec.sampling_freq/2, self.power_line_freq),
            verbose='error')

        return rec

# ______________________________________________________________________________________________________________________
    def remove_start_artifacts(self, rec):
        """ Removes self.start_time_shift percent of the recording from the beginning and self.end_time_shift from the
        end, since these parts often showed artifacts. """
        rec.duration = rec.duration - self.start_time_shift
        rec.signals = rec.signals[:, self.start_time_shift*rec.sampling_freq:]
        return rec


# ______________________________________________________________________________________________________________________
    def rename_signal_names(self, signal_names):
        signal_names = [self.electrodes_to_use[signal_name] for signal_name in signal_names]
        return signal_names

# ______________________________________________________________________________________________________________________
    def crop_data(self, rec):
        """ Only pick those specific electrodes from the recording """
        rec.signals = rec.signals[rec.signal_names]
        rec.signal_names = self.electrodes_to_use
        return rec

# ______________________________________________________________________________________________________________________
    def cut(self, rec, axis=1):
        """ only take a part of the signal as specified by self.cut  """
        rec.signals = np.take(rec.signals, np.arange(self.cut_time * rec.sampling_freq), axis=axis)
        rec.duration = self.cut_time
        return rec

# ______________________________________________________________________________________________________________________
    def clip_values(self, rec):
        """ sth is an outlier if it shows more than 800 or less than -800 microvolts amplitude. clip those outliers to
        (-800, 800) """
        # NOTE: the values cannot be exact 800 or -800 since feature computation crashes (reason and position unclear)
        # that is why we add/subtract a small amount of noise to/from the clipping value
        n = len(rec.signals[rec.signals > self.clip_value])
        noise = np.random.rand(n)
        rec.signals[rec.signals > self.clip_value] = self.clip_value
        rec.signals[rec.signals == self.clip_value] -= noise

        n2 = len(rec.signals[rec.signals < -self.clip_value])
        noise2 = np.random.rand(n2)
        rec.signals[rec.signals < -self.clip_value] = -self.clip_value
        rec.signals[rec.signals == -self.clip_value] += noise2

        return rec

# ______________________________________________________________________________________________________________________
    def volts_to_microvolts(self, rec):
        rec.signals *= 1000000
        return rec

# ______________________________________________________________________________________________________________________
    def clean(self, rec):
        # check if recording has all electrodes. abbreviate them
        rec = self.rename_electrodes(rec)

        # only take the specified signals from loaded data
        rec = self.crop_data(rec)

        # cut signal start and end to remove artifacts
        rec = self.remove_start_artifacts(rec)

        # only take the first cut seconds of the recording after the start time shift
        if self.cut_time is not None:
            rec = self.cut(rec)

        # remove power line frequency
        rec = self.filter_power_line_frequency(rec)

        # transform signal amplitudes from volts to microvolts
        rec = self.volts_to_microvolts(rec)

        # clip the values to -800, +800 microvolts
        rec = self.clip_values(rec)

        # use mne to do bandpasses to the frequency bands for synchronicity features
        rec = self.band_filter(rec)
        return rec

# ______________________________________________________________________________________________________________________
    def get_electrodes(self):
        return [electrode.strip() for electrode in self.electrodes_to_use]

# ______________________________________________________________________________________________________________________
    def __init__(self, n_jobs=1, start_time_shift=60, clip_value=800, power_line_freq=60, cut_time=None,
                 elecs=['all'], bands='0,4,8,12,18,24,30,60,120'):
        self.n_jobs = n_jobs

        self.start_time_shift = start_time_shift
        self.cut_time = cut_time

        self.clip_value = clip_value
        self.power_line_freq = power_line_freq

        self.elecs = elecs
        self.electrodes_to_use = self.take_subsets_of_elecs()

        self.bands = [int(digit) for digit in bands.split(',')]
