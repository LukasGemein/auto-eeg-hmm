from scipy.signal import hilbert
import numpy as np
import logging

from windowing import splitter
from visualization import plotting

# TODO: add visualization here instead of when reading from file


# ______________________________________________________________________________________________________________________
def compute_inst_phases(band_signals):
    """ computes instantaneous phases of band signals using the hilbert transformation
    :param band_signals: a 3 dimensional array holding for every frequency band a matrix of n_electrodes x n_samples
    :return: a 3 dimensional array holding for every frequency band a matrix of shape n_electrodes x n_samples with the
    instantaneous phases
    """
    (n_bands, n_signals, n_samples) = band_signals.shape
    instantaneous_phases = np.zeros((n_bands, n_signals, n_samples))

    for subband_id in range(n_bands):
        inst_phase = np.unwrap(np.angle(hilbert(band_signals[subband_id])))
        instantaneous_phases[subband_id] = inst_phase

    return instantaneous_phases


# ______________________________________________________________________________________________________________________
def compute_phase_lock_values(inst_phases):
    """ computes phase lock values given instantaneous phases as the distance of mean points projected on the unit
    circle
    :param inst_phases:
    :return: a 3 dimensional array holding for every frequency band a symmetrical matrix of shape n_signals x n_signals
    with pairwise phase lock values
    """
    (n_bands, n_windows, n_signals, n_samples) = inst_phases.shape
    phase_lock_values = np.ones((n_bands, n_windows, n_signals, n_signals))

    for band_id in range(n_bands):
        for window in range(n_windows):
            for electrode_id1 in range(n_signals):
                # only compute upper triangle of the synchronicity matrix and fill lower triangle with identical values
                # +1 since diagonal is always 1
                for electrode_id2 in range(electrode_id1+1, n_signals):
                    delta = inst_phases[band_id][window][electrode_id1] - inst_phases[band_id][window][electrode_id2]
                    xs, ys = np.cos(delta), np.sin(delta)
                    plv = np.sqrt(np.square(np.mean(xs)) + np.square(np.mean(ys)))
                    phase_lock_values[band_id][window][electrode_id1][electrode_id2] = plv
                    phase_lock_values[band_id][window][electrode_id2][electrode_id1] = plv

    return phase_lock_values


# ______________________________________________________________________________________________________________________
def pick_upper_triangle(phase_lock_values):
    """ since plvs are symmetrical and self-synchronicity is always 1, only pick upper triangle of the plvs
    :param phase_lock_values:
    :return: upper triangle of plvs of all frequency bands
    """
    (n_windows, n_bands, n_elecs, n_elecs) = phase_lock_values.shape
    triu_plvs = np.ndarray(shape=(n_windows, n_bands, int((n_elecs*(n_elecs-1))/2)))
    for window_id, window in enumerate(phase_lock_values):
        for band_plvs_id, band_plvs in enumerate(window):
            triu = band_plvs[np.triu(band_plvs, 1) != False]
            triu_plvs[window_id][band_plvs_id] = triu

    return triu_plvs


# ______________________________________________________________________________________________________________________
def split_into_windows(inst_phases, n_samples_in_window, overlap=0):
    # TODO: let the data splitter do this?!
    (n_freq_bands, n_elecs, n_samples) = inst_phases.shape
    # split into time windows
    windows = np.ndarray(shape=(n_freq_bands, int(n_samples/n_samples_in_window), n_elecs, n_samples_in_window))
    stride = n_samples_in_window - overlap

    # written by robin tibor schirrmeister
    for freq_band in range(n_freq_bands):
        for i_start in range(0, n_samples - n_samples_in_window + 1, stride):
            windows[freq_band] = np.take(inst_phases[freq_band], range(i_start, i_start + window_size), axis=-1)

    return np.asarray(windows)


# ______________________________________________________________________________________________________________________
def compute_features_sync(band_signals, n_samples_in_window):
    """
    :param band_signals: has shape n_bands x n_signals x n_samples. are the signals of all electrodes filtered in the
    specified frequency bands
    :return: phase lock values of all electrodes in all frequency bands
    """
    inst_phases = compute_inst_phases(band_signals)
    inst_phases = split_into_windows(inst_phases, n_samples_in_window)
    # n_windows x n_bands x n_signals x n_samples
    phase_lock_values = compute_phase_lock_values(inst_phases)
    phase_lock_values = np.swapaxes(phase_lock_values, 0, 1)

    logging.debug('before triu')
    logging.debug(phase_lock_values.shape)
    phase_lock_values = pick_upper_triangle(phase_lock_values)
    logging.debug("triu")
    logging.debug(phase_lock_values.shape)
    return phase_lock_values
