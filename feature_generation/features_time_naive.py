# Written by Lukas Gemein
# Inspired by implementation of Manuel Blum
import numpy as _np
from scipy.stats import skew as _skew
from scipy.stats import kurtosis as _kurt


def _div0(x):
    x[x < 1e-9] = 1e-9
    return x


def complexity(windows, axis):
    diff1 = _np.diff(windows)
    diff2 = _np.diff(diff1)
    sigma = _np.std(windows, axis=axis)
    sigma1 = _np.std(diff1, axis=axis)
    sigma2 = _np.std(diff2, axis=axis)
    return (sigma2 / _div0(sigma1)) / _div0((sigma1 / _div0(sigma)))


def energy(windows, axis):
    return _np.mean(windows*windows, axis=axis)


def fractaldim(windows, axis):
    diff1 = _np.diff(windows)
    sum_of_distances = _np.sum(_np.sqrt(diff1 * diff1), axis=axis)
    max_dist = _np.apply_along_axis(lambda window: _np.max(_np.sqrt(_np.square(window - window[0]))), axis, windows)
    return _np.log10(sum_of_distances) / _div0(_np.log10(max_dist))


def kurt(windows, axis):
    return _kurt(windows, axis=axis, bias=False)


def linelength(windows, axis):
    return _np.sum(_np.abs(_np.diff(windows)), axis=axis)


def max(windows, axis):
    return _np.max(windows, axis=axis)


def mean(windows, axis):
    return _np.mean(windows, axis=axis)


def median(windows, axis):
    return _np.median(windows, axis=axis)


def min(windows, axis):
    return _np.min(windows, axis=axis)


def mobility(windows, axis):
    diff1 = _np.diff(windows)
    return _np.std(diff1, axis=axis) / _div0(_np.std(windows, axis=axis))


def nonlinenergy(windows, axis):
    return _np.apply_along_axis(lambda wndw: _np.mean((_np.square(wndw[1:-1]) - wndw[2:] * wndw[:-2])), axis, windows)


def skew(windows, axis):
    return _skew(windows, axis=axis, bias=False)


def var(windows, axis):
    return _np.var(windows, axis=axis)


def zerocrossing(windows, axis):
    return _np.apply_along_axis(lambda window: _np.sum((window[:-1] <= 0) & (window[1:] > 0)), axis, windows)


def zerocrossingdev(windows, axis):
    diff1 = _np.diff(windows)
    return _np.apply_along_axis(lambda window: _np.sum(((window[:-1] <= 0) & (window[1:] > 0))), axis, diff1)
