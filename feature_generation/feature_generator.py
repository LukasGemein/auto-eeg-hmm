import numpy as np
import logging
import pyeeg
import scipy
import pywt

from feature_generation import features_time_naive as features_time
from feature_generation import features_frequency
from feature_generation import features_sync
from windowing import splitter

DOMAINS = sorted([
    'patient',
    'fft',
    'time',
    'pyeeg',
    'dwt',
    'cwt',
    'sync',
])


class FeatureGenerator(object):
    """ """

# ______________________________________________________________________________________________________________________
    def compute_dwt_feats_on_coeff_bands(self, window_coeffs):
        """ for every dwt coeff band compute mean, avg_power, std and ratio to the adjacent band
        :param window_coeffs: n_bands x n_band_coeffs
        :return: mean, avg_power, std and ratio of adjacent bands of the given bands in the window
        """
        band_means = np.ndarray(shape=(len(window_coeffs),))
        band_avg_powers = np.ndarray(shape=(len(window_coeffs),))
        band_stds = np.ndarray(shape=(len(window_coeffs),))
        for band_id, band in enumerate(window_coeffs):
            band_means[band_id] = np.mean(band)
            band_avg_powers[band_id] = np.mean(band * band)
            band_stds[band_id] = np.std(band)

        # compute the ratio of lower band to higher band. makes len(bands) - 1 values
        band_ratios = np.ndarray(shape=(len(window_coeffs) - 1,))
        for band_id in range(0, len(window_coeffs) - 1):
            band_ratios[band_id] = band_means[band_id] / band_means[band_id + 1]

        # TODO: check the ratios!
        return band_means, band_avg_powers, band_stds, band_ratios


# ______________________________________________________________________________________________________________________
    def compute_dwt_feats(self, rec_coeffs, coeff_lengths):
        """
        :param rec_coeffs: is in shape: n_windows x n_electrodes x n_all_band_coeffs
        :param coeff_lengths: a list that holds the number of coefficients belonging to each subband
        :return:
        """
        # maybe better iterate through bands as for freq feats?!
        (n_windows, n_electrodes, n_all_coeffs) = rec_coeffs.shape
        n_bands = len(coeff_lengths) + 1

        # for every time window of every electrode in this recording, compute the wavelet features and store them
        rec_means = np.ndarray(shape=(n_windows, n_electrodes, n_bands))
        rec_avg_powers = np.ndarray(shape=(n_windows, n_electrodes, n_bands))
        rec_stds = np.ndarray(shape=(n_windows, n_electrodes, n_bands))
        rec_ratios = np.ndarray(shape=(n_windows, n_electrodes, n_bands - 1))

        for window_id, window in enumerate(rec_coeffs):
            for electrode_id, electrode in enumerate(window):
                coeff_bands = np.split(electrode, coeff_lengths)

                mean, avg_power, std, ratio = self.compute_dwt_feats_on_coeff_bands(coeff_bands)

                rec_means[window_id][electrode_id] = mean
                rec_avg_powers[window_id][electrode_id] = avg_power
                rec_stds[window_id][electrode_id] = std
                rec_ratios[window_id][electrode_id] = ratio

        return rec_means, rec_avg_powers, rec_stds, rec_ratios

# ______________________________________________________________________________________________________________________
    def compute_freq_feats(self, freq_feat_name, windows_amplitudes, sfreq):
        """ Computes the feature values for a given recording. Computes the value the frequency bands as specified in
        band_limits. The values are mean over all time windows and channels
        :param freq_feat_name: the function name that should be called
        :param windows_amplitudes: ndarray of shape n_windows x n_elecs x n_amplitudes
        :param sfreq: sampling frequency
        :return: mean amplitudes in frequency bands over the different channels
        """
        func = getattr(features_frequency, freq_feat_name)
        # amplitudes shape: windows x electrodes x frequencies
        window_size = self.window_size_sec * sfreq
        freq_bin_size = sfreq / window_size

        n_bands = len(self.bands) - 1
        (n_windows, n_elecs, n_freqbins) = windows_amplitudes.shape

        amplitude_features = np.ndarray(shape=(n_bands, n_windows, n_elecs), dtype=float)
        for band_id in range(n_bands):
            lower, upper = self.bands[band_id], self.bands[band_id+1]
            # add the suggested band overlap of 50%
            if self.band_overlap:
                if band_id != 0:
                    lower -= 0.5 * (self.bands[band_id] - self.bands[band_id-1])
                if band_id != len(self.bands) - 2:
                    upper += 0.5 * (self.bands[band_id+2] - self.bands[band_id+1])

            lower_bin, upper_bin = int(lower / freq_bin_size), int(upper / freq_bin_size)
            band_amplitudes = windows_amplitudes[:, :, lower_bin:upper_bin]
            band_ampitude_features = func(band_amplitudes, axis=2)
            amplitude_features[band_id] = band_ampitude_features

        return amplitude_features

# ______________________________________________________________________________________________________________________
    def bin_power(self, X, Band, Fs):
        """ taken from pyeeg lib and adapted since on cluster slicing failed through float indexing.
        :param X: 1d signal
        :param Band: frequency band
        :param Fs: sampling frequency
        :return: power and power ratio of frequency band
        """
        C = np.fft.fft(X)
        C = abs(C)
        Power = np.zeros(len(Band) - 1)
        for Freq_Index in range(0, len(Band) - 1):
            Freq = float(Band[Freq_Index])
            Next_Freq = float(Band[Freq_Index + 1])
            Power[Freq_Index] = sum(
                C[int(Freq / Fs * len(X)): int(Next_Freq / Fs * len(X))]
            )
        Power_Ratio = Power / sum(Power)
        return Power, Power_Ratio

# ______________________________________________________________________________________________________________________
    def spectral_entropy(self, X, Band, Fs, Power_Ratio=None):
        """ taken from pyeeg lib and adapted to use self.bin_power since it returned nan
        :param X:
        :param Band:
        :param Fs:
        :param Power_Ratio:
        :return:
        """
        if Power_Ratio is None:
            Power, Power_Ratio = self.bin_power(X, Band, Fs)

        # added to catch crashes
        if len(Power_Ratio) == 1:
            return 0

        Spectral_Entropy = 0
        for i in range(0, len(Power_Ratio) - 1):
            Spectral_Entropy += Power_Ratio[i] * np.log(Power_Ratio[i])
        Spectral_Entropy /= np.log(
            len(Power_Ratio)
        )  # to save time, minus one is omitted
        return -1 * Spectral_Entropy

# ______________________________________________________________________________________________________________________
    def compute_pyeeg_feats(self, windows, sfreq):
        """ compute features implemented in pyeeg library
        :param rec: recording object holding all information
        :return: "averaged" features: power, powerratio, petrosian fractal dimension, higuche fractal dimension, hjorth
        mobility and complexity, spectral, entropy, svd entropy, fisher information, (approximate entropy, hurst
        exponent, detrended fluctuation analysis)
        """
        (n_windows, n_electrodes, n_samples_in_window) = windows.shape

        logging.debug("input windows")
        logging.debug(windows.shape)

        pwrs = np.ndarray(shape=(n_windows, n_electrodes, len(self.bands)-1,), dtype=float)
        pwrrs = np.ndarray(shape=(n_windows, n_electrodes, len(self.bands)-1,), dtype=float)

        pfds = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        hfds = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        mblts = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        cmplxts = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        ses = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        svds = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        fis = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        apes = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        hrsts = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)
        dfas = np.ndarray(shape=(n_windows, n_electrodes, 1), dtype=float)

        # these values are taken from the tuh paper
        TAU, DE, Kmax = 4, 10, 5
        # for every time window of every electrode fo this recording compute the features and store values in a list
        for window_id, window in enumerate(windows):
            for window_electrode_id, window_electrode in enumerate(window):
                # taken from pyeeg code / paper
                electrode_diff = list(np.diff(window_electrode))
                M = pyeeg.embed_seq(window_electrode, TAU, DE)
                W = scipy.linalg.svd(M, compute_uv=False)
                W /= sum(W)

                power, power_ratio = self.bin_power(window_electrode, self.bands, sfreq)
                pwrs[window_id][window_electrode_id] = power
                # mean of power ratio is 1/(len(self.bands)-1)
                pwrrs[window_id][window_electrode_id] = power_ratio

                pfd = pyeeg.pfd(window_electrode, electrode_diff)
                pfds[window_id][window_electrode_id] = pfd

                hfd = pyeeg.hfd(window_electrode, Kmax=Kmax)
                hfds[window_id][window_electrode_id] = hfd

                mobility, complexity = pyeeg.hjorth(window_electrode, electrode_diff)
                mblts[window_id][window_electrode_id] = mobility
                cmplxts[window_id][window_electrode_id] = complexity

                se = self.spectral_entropy(window_electrode, self.bands, sfreq, power_ratio)
                ses[window_id][window_electrode_id] = se

                svd = pyeeg.svd_entropy(window_electrode, TAU, DE, W=W)
                svds[window_id][window_electrode_id] = svd

                fi = pyeeg.fisher_info(window_electrode, TAU, DE, W=W)
                fis[window_id][window_electrode_id] = fi

                # this crashes...
                # ape = pyeeg.ap_entropy(electrode, M=10, R=0.3*np.std(electrode))
                # apes.append(ape)

                # takes very very long to compute
                # hurst = pyeeg.hurst(electrode)
                # hrsts.append(hurst)

                # takes very very long to compute
                # dfa = pyeeg.dfa(electrode)
                # dfas.append(dfa)

        return pwrs, pwrrs, pfds, hfds, mblts, cmplxts, ses, svds, fis, apes, hrsts, dfas


# ______________________________________________________________________________________________________________________
    def generate_features(self, rec):
        """ computes features. returns one feature vector per recording
        :param rec: rec.signals has shape n_windows x n_electrodes x n_samples_in_window
                    rec.signals_ft has shape n_windows x n_electrodes x n_samples_in_window/2 + 1
        :return: the feature vector of that recording
        """
        windows = self.splitter.split(rec)
        (n_windows, n_elecs, n_samples_in_window) = windows.shape
        sfreq = rec.sampling_freq

        features = []

########################################################################################################################
        # computes 3*n_elecs*n_bands + n_elecs*(n_bands-1) features
        # in shape n_windows x n_elecs x n_cwt_band_feats * n_bands + n_cwt_ratio_features * n_bands
        if self.cwt_feat_flag:
            n_bands = len(self.bands) - 1
            coefficients = np.ndarray(shape=(n_windows, n_elecs, n_bands, n_samples_in_window))
            for window_id, window in enumerate(windows):
                for elec_id, elec in enumerate(window):
                    coef, freqs = pywt.cwt(
                        data=elec,
                        scales=[50.75, 25.375, 15.622, 11.282, 8.463, 6.77, 3.3854, 1.6927],
                        wavelet='morl',
                        sampling_period=1 / sfreq
                    )
                    coefficients[window_id][elec_id] = coef
            logging.debug("coefficients")
            logging.debug(coefficients.shape)

            wavelet_feats = np.ndarray(shape=(len(self.cwt_feats) - 1, n_windows, n_elecs, n_bands))
            wavelet_feats[0] = np.mean(coefficients, axis=-1)
            wavelet_feats[1] = np.mean(coefficients * coefficients, axis=-1)
            wavelet_feats[2] = np.std(coefficients, axis=-1)
            logging.debug("wavelet feats")
            logging.debug(wavelet_feats.shape)

            band_ratios = np.ndarray(shape=(n_bands - 1, n_windows, n_elecs))
            for band_id in range(0, n_bands - 1):
                # wavelet feats at position 0 is mean
                band_ratios[band_id] = wavelet_feats[0][:, :, band_id] / wavelet_feats[0][:, :, band_id + 1]

            band_ratios = np.rollaxis(band_ratios, 0, 3)
            logging.debug("ratios")
            logging.debug(band_ratios.shape)

            # shape windows x elecs x band_feats x bands
            wavelet_feats = np.rollaxis(wavelet_feats, 0, 3)
            logging.debug('rolled')
            logging.debug(wavelet_feats.shape)

            wavelet_feats = np.reshape(wavelet_feats, (n_windows, n_elecs, n_bands * (len(self.cwt_feats) - 1)))
            logging.debug("reshaped")
            logging.debug(wavelet_feats.shape)

            cwt_feats = np.concatenate((wavelet_feats, band_ratios), axis=2)
            logging.debug('all')
            logging.debug(cwt_feats.shape)

            print("cwt", cwt_feats.shape)
            features.append(cwt_feats)


########################################################################################################################
        # TODO: test
        # dwt features
        # computes 3*n_elecs*n_lvls + n_elecs*(n_lvls-1) features
        # in shape n_windows x n_elecs x n_dwt_band_features * (n_lvls+1) + n_dwt_ratio_features * n_lvls
        if self.dwt_feat_flag:
            # dwt_feats = np.ndarray(shape=(n_windows, n_elecs, 3*(level+1) + level))
            wavelet_feats1 = np.ndarray(shape=(len(self.dwt_feats)-1, n_windows, n_elecs, self.dwt_level+1))
            wavelet_feats2 = np.ndarray(shape=(n_windows, n_elecs, self.dwt_level))
            # given a sampling frequency of 250 hz we will use a level 5 dwt, since
            # 125 - 65.5 - 32.75 - 16.375 - 8.1875 - 4.09375
            list_of_rec_coeffs = pywt.wavedec(windows, wavelet=self.wavelet, level=self.dwt_level, axis=2)
            # for every subband, store how many coeffs it has
            coeff_lengths = np.cumsum([r.shape[-1] for r in list_of_rec_coeffs][:-1])
            # correct the weird shape of the returned list of rec coeffs
            # rec_coeffs is in shape: n_windows x n_electrodes x n_all_band_coeffs
            rec_coeffs = np.dstack(list_of_rec_coeffs)

            means, avg_powers, stds, ratios = self.compute_dwt_feats(rec_coeffs, coeff_lengths)
            wavelet_feats1[:] = means, avg_powers, stds
            wavelet_feats2[:] = ratios

            logging.debug("dwt")
            logging.debug("mean, avg_power, std")
            logging.debug(wavelet_feats1.shape)
            logging.debug("ratio")
            logging.debug(wavelet_feats2.shape)

            # shape windows x elecs x band_feats x level + 1
            wavelet_feats1 = np.rollaxis(wavelet_feats1, 0, 3)
            logging.debug("rolled")
            logging.debug(wavelet_feats1.shape)
            wavelet_feats1 = np.reshape(wavelet_feats1, (n_windows, n_elecs, (self.dwt_level+1)*(len(self.dwt_feats)-1)))
            logging.debug(wavelet_feats1.shape)

            dwt_feats = np.concatenate((wavelet_feats1, wavelet_feats2), axis=2)
            logging.debug("final")
            logging.debug(dwt_feats.shape)

            print("dwt", dwt_feats.shape)
            features.append(dwt_feats)


########################################################################################################################
        # frequency features
        # will be in shape n_windows x n_elecs x n_bands x n_freq_features
        if self.freq_feat_flag:
            freq_feats = np.ndarray(shape=(len(self.freq_feats), len(self.bands) - 1, n_windows, n_elecs))
            windows_amplitudes = np.abs(np.fft.rfft(windows, axis=2))

            for freq_feat_id, freq_feat_name in enumerate(self.freq_feats):
                freq_feats[freq_feat_id] = self.compute_freq_feats(freq_feat_name, windows_amplitudes, sfreq)

            logging.debug('frequency')
            logging.debug(freq_feats.shape)
            freq_feats = np.swapaxes(freq_feats, 0, 2)
            freq_feats = np.swapaxes(freq_feats, 2, 3)
            freq_feats = np.swapaxes(freq_feats, 1, 2)
            logging.debug(freq_feats.shape)
            # freq_feats = np.reshape(freq_feats, (n_windows, n_elecs, len(self.freq_feats)*(len(self.bands)-1)))
            logging.debug(freq_feats.shape)

            print("freq", freq_feats.shape)
            features.append(freq_feats)


########################################################################################################################
        # add medication, ...?
        # patient features
        # will be in shape n_patient_features,
        if self.patient_feat_flag:
            patient_feats = np.ndarray(shape=(len(self.patient_feats),))
            patient_feats[0] = rec.sex
            patient_feats[1] = rec.age

            logging.debug('patient')
            logging.debug(patient_feats.shape)

            print("patient", patient_feats.shape)
            features.append(patient_feats)


########################################################################################################################
        # computes 7*n_elec + 2*n_bands*n_elecs values
        # in shape n_windows x n_elecs x n_bands * n_pyeeg_freq_features  + (n_bands-1) * n_pyeeg_time_features
        if self.pyeeg_feat_flag:
            pyeeg_features = self.compute_pyeeg_feats(windows, sfreq)
            [pwrs, pwrrs, pfds, hfds, mblts, cmplxts, ses, svds, fis, apes, hrsts, dfas] = pyeeg_features

            logging.debug("pwr shape")
            logging.debug(pwrs.shape)
            logging.debug("pwrr shape")
            logging.debug(pwrrs.shape)
            logging.debug("cmplxts shape")
            logging.debug(cmplxts.shape)
            logging.debug("fis shape")
            logging.debug(fis.shape)

            pyeeg_features = np.concatenate((pwrs, pwrrs, pfds, hfds, mblts, cmplxts, ses, svds, fis), axis=-1)
            logging.debug("total")
            logging.debug(pyeeg_features.shape)

            print("pyeeg", pyeeg_features.shape)
            features.append(pyeeg_features)


########################################################################################################################
        # synchronicity features
        # computed n_bands*(n_elecs*(n_elecs-1)/2) features
        if self.sync_feat_flag:
            phase_lock_values = features_sync.compute_features_sync(rec.signals_band_filtered, n_samples_in_window)
            logging.debug("sync")
            logging.debug(phase_lock_values.shape)
            phase_lock_values = np.swapaxes(phase_lock_values, 1, 2)

            print("sync", phase_lock_values.shape)
            features.append(phase_lock_values)


########################################################################################################################
        # rewritten and tested to compute the same as before
        # will be in shape n_windows x n_elecs x n_time_features
        if self.time_feat_flag:
            time_feats = np.ndarray(shape=(len(self.time_feats), n_windows, n_elecs))
            for time_feat_id, time_feat_name in enumerate(self.time_feats):
                func = getattr(features_time, time_feat_name)
                time_feats[time_feat_id] = func(windows, -1)

            logging.debug('time')
            logging.debug(time_feats.shape)
            time_feats = np.swapaxes(time_feats, 0, 2)
            time_feats = np.reshape(time_feats, (n_windows, n_elecs, len(self.time_feats)))
            logging.debug(time_feats.shape)

            print("time", time_feats.shape)
            features.append(time_feats)

            # HOW TO COMBINE FREQ AND TIME FEATS WITHOUT COLLAPSING WINDOW DIM!
            # freq_feats_and_time_feats = np.concatenate((freq_feats, time_feats), axis=-1)
            # print(freq_feats_and_time_feats.shape)

        return features

# ______________________________________________________________________________________________________________________
    def get_feature_labels(self):
        """ create a list of feature labels that can be used to identify the feature values later on """
        if self.feature_labels is None:
            feature_labels = list()

            # electrode names
            if self.electrodes is not None:
                electrodes = self.electrodes
            else:
                electrodes = np.linspace(1, 21, 21)
                electrodes = ['ch'+str(electrode) for electrode in electrodes]

            # cwt features
            if self.cwt_feat_flag:
                for electrode in electrodes:
                    for cwt_feat in self.cwt_feats:
                        if cwt_feat != 'ratio':
                            for band_id, band in enumerate(self.bands[:-1]):
                                label = '_'.join(['cwt', cwt_feat, str(band) + '-' + str(self.bands[band_id + 1])
                                                  + 'Hz', str(electrode)])
                                feature_labels.append(label)

                        else:
                            for band_id, band in enumerate(self.bands[:-2]):
                                label = '_'.join(['cwt', cwt_feat, str(band) + '-' + str(self.bands[band_id + 1]) + '-'
                                                  + str(self.bands[band_id+2]) + 'Hz', str(electrode)])
                                feature_labels.append(label)

            # dwt features
            if self.dwt_feat_flag:
                dwt_bands = ["a5", "d5", "d4", "d3", "d2", "d1"]
                for electrode in electrodes:
                    for dwt_feat in self.dwt_feats:
                        if dwt_feat != 'ratio':
                            for band in dwt_bands:
                                feature_labels.append('_'.join(['dwt', dwt_feat, band, str(electrode)]))

                        else:
                            for band_id in range(0, len(dwt_bands) - 1):
                                feature_labels.append(
                                    '_'.join(['dwt', dwt_feat, str(dwt_bands[band_id]) + '-' +
                                              str(dwt_bands[band_id + 1]), electrode]))

            # fft features
            if self.freq_feat_flag:
                for electrode in electrodes:
                    for band_id, band in enumerate(self.bands[:-1]):
                        for freq_feat in self.freq_feats:
                            label = '_'.join(['fft', freq_feat, str(band) + '-' + str(self.bands[band_id+1]) + 'Hz',
                                              str(electrode)])
                            feature_labels.append(label)

            # patient features
            if self.patient_feat_flag:
                for patient_feat in self.patient_feats:
                    feature_labels.append('_'.join(['patient', patient_feat]))

            # pyeeg features
            if self.pyeeg_feat_flag:
                for electrode in electrodes:
                    for pyeeg_feat in self.pyeeg_feats:
                        if pyeeg_feat == "pwr" or pyeeg_feat == "pwrr":
                            for band_id, band in enumerate(self.bands[:-1]):
                                label = '_'.join(['pyeeg', pyeeg_feat, str(band) + '-' + str(self.bands[band_id + 1])
                                                  + 'Hz', str(electrode)])
                                feature_labels.append(label)
                        else:
                            label = '_'.join(['pyeeg', pyeeg_feat, str(electrode)])
                            feature_labels.append(label)

            # synchronicity features
            if self.sync_feat_flag:
                for electrode_id, electrode in enumerate(electrodes):
                    for electrode_id2 in range(electrode_id+1, len(electrodes)):
                        for band_id, band in enumerate(self.bands[:-1]):
                                label = '_'.join(['sync', 'plv', str(band) + '-' + str(self.bands[band_id+1]) + 'Hz',
                                                  str(electrode) + '-' + electrodes[electrode_id2]])
                                feature_labels.append(label)

            # time features
            if self.time_feat_flag:
                for electrode in electrodes:
                    for time_feat in self.time_feats:
                        label = '_'.join(['time', time_feat, str(electrode)])
                        feature_labels.append(label)

            self.feature_labels = np.asarray(feature_labels)

        return self.feature_labels

# ______________________________________________________________________________________________________________________
    def __init__(self, domain, bands, window='boxcar', window_size_sec=2, overlap=0, band_overlap=None, elecs=None):
        # all the features that are implemented
        self.patient_feats = ["sex", "age"]
        self.freq_feats = sorted([feat_func for feat_func in dir(features_frequency) if not feat_func.startswith('_')])
        self.time_feats = sorted([feat_func for feat_func in dir(features_time) if not feat_func.startswith('_')])
        pyeeg_freq_feats = ["pwr", "pwrr"]
        pyeeg_time_feats = ["pfd", "hfd", "mblt", "cmplxt", "se", "svd", "fi"]  # , "ape", "hrst", "dfa"]
        self.pyeeg_feats = pyeeg_freq_feats + pyeeg_time_feats
        self.sync_feats = ['plv']
        self.dwt_feats = ["mean", "avg-power", "std", "ratio"]
        self.cwt_feats = ["mean", "avg-power", "std", "ratio"]

        # toggle computation of feature sub groups
        self.cwt_feat_flag = True if 'cwt' in domain or 'all' in domain else False
        self.dwt_feat_flag = True if 'dwt' in domain or 'all' in domain else False
        self.freq_feat_flag = True if 'fft' in domain or 'all' in domain else False
        self.patient_feat_flag = True if 'patient' in domain or 'all' in domain else False
        self.pyeeg_feat_flag = True if 'pyeeg' in domain or 'all' in domain else False
        self.sync_feat_flag = True if 'sync' in domain or 'all' in domain else False
        self.time_feat_flag = True if 'time' in domain or 'all' in domain else False

        # cmd args needed for computation
        self.window_size_sec = window_size_sec
        self.overlap = overlap
        self.bands = [int(digit) for digit in bands.split(',')]
        self.band_overlap = band_overlap

        # information that is needed by other objects / written to file
        self.electrodes = elecs
        self.feature_labels = None
        self.feature_names = None

        # given 250 hz sfreq this results in 125 - 65.5 - 32.75 - 16.375 - 8.1875 - 4.09375 hz bands
        self.dwt_level = 5
        self.wavelet = pywt.Wavelet('db4')

        self.splitter = splitter.DataSplitter(window=window, window_size_sec=window_size_sec)
