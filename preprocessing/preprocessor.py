import multiprocessing as mp
import numpy as np
import logging

from feature_generation import feature_generator
from preprocessing import recording
from cleaning import cleaner
from utils import io


class Preprocessor(object):
    """ The preprocessor takes raw input data and returns features. Thereby it cleans and splits the data. The
    processing is parallelized, s.t. reading of data, cleaning, splitting, fourier transform, visualization and feature
    computation are faster.
    """

# ______________________________________________________________________________________________________________________
    def clean_and_generate_features(self, rec):
        """ the function that is executed for every recording in the in_q
        :param rec: the recording
        :return: the features of that recording and the number of time windows (to maybe combine window features after
        prediction)
        """
        short_name = '...'+'/'.join(rec.name.split('/')[-4:])
        logging.info("\tProcessing {}".format(short_name))

        rec.signals = io.load_data_with_mne(rec)
        rec = self.cleaner.clean(rec)
        features = self.feature_generator.generate_features(rec)

        return features

# ______________________________________________________________________________________________________________________
    def worker(self, in_q, out_q, func):
        """ The worker will take one item of the in_q and will work on the element as specified in
        clean_split_transform_visualize_featurize. It then puts the results back to the out_q
        :param in_q: the queue that holds all the recordings that should be processed
        :param out_q: the queue that holds the results of all processed recordings
        :param func: the function that is run for to process every recording
        """
        while not in_q.empty():
            item, ident = in_q.get(timeout=1)
            out_q.put((func(item), ident))

# ______________________________________________________________________________________________________________________
    def random_subset(self, edf_files, subset):
        """ if cmd_args.subset is set, don't read all the recordings but take a random subset of the specified size
        :param edf_files: list of all recordings on the hdd
        :param subset: the size of the subset
        :return: a list of randomly selected files of the specified size
        """
        ids = np.arange(len(edf_files))
        np.random.shuffle(ids)
        subset_ids = ids[:subset]

        return [edf_files[i] for i in sorted(subset_ids)]

# ______________________________________________________________________________________________________________________
    def add_recording_to_queue(self, in_dir, edf_files, in_q):
        """ for ech recording that was read,
        :param in_dir: the input directory which is used to extract the class of the recording
        :param edf_files: list of recording paths
        :param in_q: the queue that holds the recordings for processing
        :return: the queue that holds the recordings for parallel processing, the number of recordings that are suitable
        for processing, e.g. that could be opened and read, has an appropriate length, etc
        """
        edf_count = 0
        for edf_ident, edf_file in enumerate(edf_files):
            rec = recording.Recording(in_dir, edf_file, *io.get_info_with_mne(edf_file))

            rec.sex, rec.age = io.get_patient_info(rec.name)
            self.recording_names[in_dir].append('/'.join(rec.name.split('/')[-3:]))

            in_q.put((rec, edf_ident))
            edf_count += 1

        return in_q, edf_count

# ______________________________________________________________________________________________________________________
    def read_files(self, in_dir, subset):
        """ first, read all files from the given directory, then if a subset is desired, create a random subset. return
        a list of file names
        """
        edf_files = io.read_all_file_names(in_dir, extension='.edf')

        if subset is not None:
            edf_files = self.random_subset(edf_files, subset)

        return edf_files

# ______________________________________________________________________________________________________________________
    def spawn_start_join_processes(self, cmd_args, in_q, out_q):
        processes = [mp.Process(target=self.worker, args=(in_q, out_q, self.clean_and_generate_features))
                     for i in range(cmd_args.n_proc)]

        for process in processes:
            process.start()

        for process in processes:
            process.join()

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q, edf_count):
        """ catch the results returned by the individual parallel processes
        :param out_q: the queue holding the results
        :param edf_count: the number of edfs processed of that class. this is needed to create the feature matrix of
        the correct size and to assign each feature vector to the correct position in the matrix (the order in which
        they were read from the hdd (sorted by date))
        :return: the feature matrix
        """
        features = edf_count * [None]
        while not out_q.empty():
            values, ident = out_q.get()
            features[ident] = values

        return features

# ______________________________________________________________________________________________________________________
    def init_preprocessing_units(self, cmd_args):
        self.cleaner = cleaner.DataCleaner(
            cut_time=cmd_args.cut,
            elecs=cmd_args.elecs,
            bands=cmd_args.bands
        )

        self.feature_generator = feature_generator.FeatureGenerator(
            domain=cmd_args.domain,
            bands=cmd_args.bands,
            window_size_sec=cmd_args.windowsize,
            elecs=self.cleaner.get_electrodes(),
            window=cmd_args.window
        )

# ______________________________________________________________________________________________________________________
    def preprocess(self, cmd_args):
        """ Checks if all the EEG recordings of the input directories can be processed. For every processable recording
        an entry in the multiprocessing queue is inserted. Results (features) are written to .hdf5 files per input dir.
        :param cmd_args:
        :return:
        """
        self.init_preprocessing_units(cmd_args)

        # create output directory
        io.check_out(cmd_args.output, cmd_args.input)
        io.write_feature_labels(cmd_args.output, self.feature_generator.get_feature_labels())

        # set up multiprocessing
        m = mp.Manager()
        in_q, out_q = m.Queue(), m.Queue()

        # stores for every class the hdf5 file name where features are stored
        feature_directories = []
        for in_dir_id, in_dir in enumerate(cmd_args.input):
            self.recording_names[in_dir] = list()

            # read all edf files from directory
            edf_files = self.read_files(in_dir, cmd_args.subset)

            # for every recording creates an item in the multiprocessing queue
            in_q, edf_count = self.add_recording_to_queue(in_dir, edf_files, in_q)

            # start the processes
            self.spawn_start_join_processes(cmd_args, in_q, out_q)

            # take results from feature computation from multiprocessing output queue
            features = self.catch_results(out_q, edf_count)

            # make an ndarray that has for every recording and every feature domain an ndarray with its features
            arr = np.ndarray(shape=(len(edf_files), len(cmd_args.domain), ), dtype=np.ndarray)
            for i in range(len(edf_files)):
                for j in range(len(cmd_args.domain)):
                    arr[i][j] = features[i][j]

            print(arr.shape)

            # store the individual domain feature ndarrays to hdf5 files
            for domain_id, domain in enumerate(cmd_args.domain):
                print("domain", domain, arr[:, domain_id].shape)
                directory = io.write_domain_hdf5(arr[:, domain_id], in_dir, cmd_args, domain)

            # write files to hdf5 and save their directory
            # hacky but last directory works
            feature_directories.append(directory)

        # also write the names of processed recordings to file for backtracking purposes
        io.write_recording_names(cmd_args.output, cmd_args.input, self.recording_names)

        return feature_directories

# ______________________________________________________________________________________________________________________
    def __init__(self):
        self.cleaner = None
        self.feature_generator = None

        self.recording_names = {}
