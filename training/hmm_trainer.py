from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
from hmmlearn.hmm import GMMHMM
import multiprocessing as mp
import numpy as np
import logging

from utils import io

NORMAL_LABEL = 0
ABNORMAL_LABEL = 1
SEED = 4316932

# TODO: add picking subset from feature data
# TODO: don't use majority vote


class HMMTrainer(object):
    """ makes use of hmms to train and predict samples. one hmm is created for every electrode and every class. """

# ______________________________________________________________________________________________________________________
    def get_left_to_right_transmat_and_startprob(self, n_components):
        """ fill the transition matrix to represent a left-to-right hmm """
        transmat = np.zeros((n_components, n_components))
        # Left-to-right: each state is connected to itself and its direct successor.
        for i in range(n_components):
            if i == n_components - 1:
                transmat[i, i] = 1.0
            else:
                transmat[i, i] = transmat[i, i + 1] = 0.5

        startp = np.zeros(n_components)
        startp[0] = 1
        return transmat, startp

# ______________________________________________________________________________________________________________________
    def initialize_model(self, data, lengths, n_components=3, n_mix=3):
        """ initialize a left-to-right model with n_components and n_mix gaussian output
        :param data: feature matrix
        :param lengths: list of integers that states how many samples in the feature matrix belong together
        :param n_components: number of hidden states
        :param n_mix: number of gaussian mixtures
        :return: a fitted hmm
        """
        # TODO: initialize several hmms since they can be stuck in local maxima.
        # TODO: take the one with highest score
        # as TUH create a gmmhmm with 3 hidden states and 3 gaussian mixtures output
        hmm = GMMHMM(
            n_components=n_components,
            n_mix=n_mix,
            random_state=np.random.RandomState(seed=SEED),
            n_iter=120,
            tol=1
        )
        # as TUH create a left-to-right hmm
        hmm.transmat, hmm.startprob = self.get_left_to_right_transmat_and_startprob(n_components)
        # fit the specified hmm to the observations
        hmm = hmm.fit(
            X=data,
            lengths=lengths
        )
        return hmm

# ______________________________________________________________________________________________________________________
    def train_models(self, train_data):
        """ for every electrode, create and fit a hmm
        :param train_data: feature matrix
        :return: an array of fitted models
        """
        (n_files, n_windows, n_bands, n_features, n_elecs) = train_data.shape

        # create an array that will hold a fitted gmmhmm for every electrode
        models = np.ndarray(shape=(n_elecs,), dtype=GMMHMM)
        for elec_id in range(n_elecs):
            logging.info("\t\tFitting electrode {}".format(elec_id))
            elec_train_data = train_data[:, :, :, :, elec_id]
            elec_train_data = elec_train_data.reshape((n_files * n_windows, n_bands * n_features))
            models[elec_id] = self.initialize_model(elec_train_data, lengths=n_files * [n_windows])

        return models

# ______________________________________________________________________________________________________________________
    def classify_sample(self, normal_model, abnormal_model, sample):
        """ use the fitted hmms to score a sample. highest score wins """
        # tie breaking in favor of normal
        return NORMAL_LABEL if normal_model.score(sample) >= abnormal_model.score(sample) else ABNORMAL_LABEL

# ______________________________________________________________________________________________________________________
    def predict(self, normal_models, abnormal_models, test_data):
        """ for every test fold classify the sample in each test fold, store the winning class
        :param normal_models: list of normal models fitted by electrode
        :param abnormal_models: list of abnormal models fitted by electrode
        :param test_data: matrix holding test fold data
        :return: array of predictions
        """
        (n_files, n_windows, n_bands, n_features, n_elecs) = test_data.shape

        predictions = np.ndarray(shape=(n_files, n_elecs), dtype=int)
        for test_sample_id, test_sample in enumerate(test_data):
            for elec_id in range(n_elecs):
                elec_test_data = test_sample[:, :, :, elec_id]
                elec_test_data = elec_test_data.reshape((n_windows, n_bands * n_features))
                # order of models matters! normal first!
                prediction = self.classify_sample(
                    normal_models[elec_id],
                    abnormal_models[elec_id],
                    elec_test_data)
                predictions[test_sample_id][elec_id] = prediction

        return predictions

# ______________________________________________________________________________________________________________________
    def per_electrode_accuracy(self, predictions, label):
        """ compute how many electrodes were assigned to the correct class """
        acc = np.sum(predictions == label, axis=0) / len(predictions)
        mean_acc = np.mean(acc)

        return acc, mean_acc

# ______________________________________________________________________________________________________________________
    def majority_vote_over_electrodes(self, predictions):
        """ calculate the majority label over all electrodes to find the sample label """
        normal_choices = np.sum(predictions == NORMAL_LABEL, axis=1)
        abnormal_choices = np.sum(predictions == ABNORMAL_LABEL, axis=1)

        # tie breaking in favor of normal
        majority_predictions = np.asarray([NORMAL_LABEL if normal_choices[i] >= abnormal_choices[i]
                                           else ABNORMAL_LABEL for i in range(len(normal_choices))])

        # return np.sum(majority_predictions == label) / len(majority_predictions)
        return majority_predictions

# ______________________________________________________________________________________________________________________
    def read_features(self, input_dirs):
        """ read feature matrices from hdf5 files """
        datas = []
        for input_dir in input_dirs:
            datas.append([])
            hdf5_files = io.read_all_file_names(input_dir, '.hdf5', key='natural')
            for hdf5_file in hdf5_files:
                data = io.read_domain_hdf5(hdf5_file)
                datas[-1].append(data)
                print("reading in trainer", len(data), data[-1].shape)

        return datas

# ______________________________________________________________________________________________________________________
    def read_features_from_file(self, inputs):
        """ read feature matrices from hdf5 file """
        datas = len(inputs) * [None]
        for in_id, in_file in enumerate(inputs):
            data = io.read_hdf5(in_file)
            # swap n_windows and n_features to get a better order for training of hmms
            datas[in_id] = np.swapaxes(data, 1, 3)
        return datas

# ______________________________________________________________________________________________________________________
    def train_test_split(self, n_samples):
        """ generate ids to split the input feature matrix in train and test subsets """
        ids = np.arange(n_samples)
        train_fold_ids, test_fold_ids = [], []
        kf = KFold(n_splits=self.n_folds)
        for train, test in kf.split(ids):
            train_fold_ids.append(train)
            test_fold_ids.append(test)

        return np.asarray(train_fold_ids), np.asarray(test_fold_ids)

# ______________________________________________________________________________________________________________________
    def scale_features(self, normal_train, abnormal_train, normal_test, abnormal_test):
        """ standardize features """
        # TODO: this is very ugly. improve this!
        # these are 5D arrays, reshape to 2D
        normal_train_shape = normal_train.shape
        (n_files, n_windows, n_bands, n_features, n_elecs) = normal_train.shape
        normal_train = np.reshape(normal_train, (n_files*n_windows, n_bands*n_features*n_elecs))

        abnormal_train_shape = abnormal_train.shape
        (n_files, n_windows, n_bands, n_features, n_elecs) = abnormal_train.shape
        abnormal_train = np.reshape(abnormal_train, (n_files*n_windows, n_bands*n_features*n_elecs))

        normal_test_shape = normal_test.shape
        (n_files, n_windows, n_bands, n_features, n_elecs) = normal_test.shape
        normal_test = np.reshape(normal_test, (n_files*n_windows, n_bands*n_features*n_elecs))

        abnormal_test_shape = abnormal_test.shape
        (n_files, n_windows, n_bands, n_features, n_elecs) = abnormal_test.shape
        abnormal_test = np.reshape(abnormal_test, (n_files*n_windows, n_bands*n_features*n_elecs))

        # fit the scaler on the very first train fold data, transform all other
        if self.scaler is None:
            self.scaler = StandardScaler(copy=False)
            normal_train = self.scaler.fit_transform(normal_train)
        else:
            normal_train = self.scaler.transform(normal_train)
        abnormal_train = self.scaler.transform(abnormal_train)
        normal_test = self.scaler.transform(normal_test)
        abnormal_test = self.scaler.transform(abnormal_test)

        # reshape back to 2D for electrode indexing in train models...
        normal_train = np.reshape(normal_train, normal_train_shape)
        abnormal_train = np.reshape(abnormal_train, abnormal_train_shape)
        normal_test = np.reshape(normal_test, normal_test_shape)
        abnormal_test = np.reshape(abnormal_test, abnormal_test_shape)

        return normal_train, abnormal_train, normal_test, abnormal_test

# ______________________________________________________________________________________________________________________
    def train_and_predict_fold(self, normal_train, abnormal_train, normal_test, abnormal_test):
        """ trains n_elecs models for both normal and abnormal train data. evaluates the test data by scoring each
        sample with the normal and abnormal model of the elctrode. highest score wins. computes metrics accuracy,
        precision, recall and f1 score
        :return: results in shape n_classes x n_metrics
        """
        # standardize features
        normal_train, abnormal_train, normal_test, abnormal_test = self.scale_features(normal_train, abnormal_train,
                                                                                       normal_test, abnormal_test)

        # models have shape (n_elecs, )
        normal_models = self.train_models(normal_train)
        abnormal_models = self.train_models(abnormal_train)

        normal_predictions = self.predict(normal_models, abnormal_models, normal_test)
        abnormal_predictions = self.predict(normal_models, abnormal_models, abnormal_test)

        normal_predictions = self.majority_vote_over_electrodes(normal_predictions)
        abnormal_predictions = self.majority_vote_over_electrodes(abnormal_predictions)

        predictions = np.concatenate((normal_predictions, abnormal_predictions), axis=0)
        labels = len(normal_predictions) * [NORMAL_LABEL] + len(abnormal_predictions) * [ABNORMAL_LABEL]

        acc = accuracy_score(labels, predictions)
        prec = precision_score(labels, predictions)
        rec = recall_score(labels, predictions)
        f1 = f1_score(labels, predictions)

        return [acc, prec, rec, f1]

# ______________________________________________________________________________________________________________________
    def worker(self, in_q, out_q, func):
        while not in_q.empty():
            normal_train, abnormal_train, normal_test, abnormal_test, fold_id = in_q.get(timeout=1)
            out_q.put((func(normal_train, abnormal_train, normal_test, abnormal_test), fold_id))

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ store results returned from multiprocessing in correct order through fold_id """
        results = out_q.qsize() * [None]
        while not out_q.empty():
            fold_results, fold_id = out_q.get()
            results[fold_id] = fold_results
        return results

# ______________________________________________________________________________________________________________________
    def cross_validate(self, normal_data, normal_train_fold_ids, normal_test_fold_ids,
                       abnormal_data, abnormal_train_fold_ids, abnormal_test_fold_ids):
        """ in every cv fold fit an hmm per electrode on train fold data and predict test fold data
        :param xxx_data: feature matrix
        :param xxx_train_fold_ids: list of indices to pick the train folds from the feature matrix
        :param xxx_test_fold_ids: list of indices to pick the test folds from the feature matrix
        """
        # set up multiprocessing
        manager = mp.Manager()
        in_q, out_q = manager.Queue(), manager.Queue()
        processes = np.ndarray(shape=(self.n_folds,), dtype=mp.Process)

        # create n_proc many jobs to be processed in parallel where every process runs a single of n_folds cv folds
        worked_folds = 0
        while worked_folds < self.n_folds:
            for process_id in range(worked_folds, min(worked_folds+self.n_proc, self.n_folds)):
                normal_train = normal_data[normal_train_fold_ids[process_id]]
                abnormal_train = abnormal_data[abnormal_train_fold_ids[process_id]]

                normal_test = normal_data[normal_test_fold_ids[process_id]]
                abnormal_test = abnormal_data[abnormal_test_fold_ids[process_id]]

                in_q.put((normal_train, abnormal_train, normal_test, abnormal_test, process_id-worked_folds))
                processes[process_id] = mp.Process(target=self.worker, args=(in_q, out_q, self.train_and_predict_fold))

            for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_proc]):
                logging.info("\tTraining and predicting fold {}.".format(worked_folds+process_id))
                process.start()

            for process in processes[worked_folds:worked_folds+self.n_proc]:
                process.join()

            self.results[worked_folds:worked_folds+self.n_proc] = self.catch_results(out_q)
            worked_folds += self.n_proc

# ______________________________________________________________________________________________________________________
    def train(self, inputs):
        """ read features from hdf5 file, split into train and test folds and perform 10 fold cv """
        # n_files x n_windows x n_bands x n_features x n_elecs
        [normal_data, abnormal_data] = self.read_features(inputs)  # TODO: THIS NEEDS TO BE FIXED NEXT
        n_files = len(normal_data)
        n_windows = len(normal_data[0])
        n_elecs = len(normal_data[0][0][0])
        for domain_id, domain in enumerate(normal_data):
            # patient domain cannot be reshaped in n_windows and such, so skip it
            if not len(domain.shape) > 2:
                continue

            if len(domain.shape) > 4:
                normal_data[domain_id] = np.reshape(normal_data[domain_id], (n_files, n_windows, -1))
                print("fixing shape", normal_data[domain_id].shape)

            normal_data[domain_id] = np.median(normal_data[domain_id], axis=1)
            # print("after median", normal_data[domain_id].shape)
            normal_data[domain_id] = np.vstack(normal_data[domain_id])
            print("after stacking", normal_data[domain_id].shape)



            #
            # normal_data[domain_id] = np.reshape(domain, (3, 30, -1))
            # print(normal_data[domain_id].shape)

        print
        exit()

        # [normal_data, abnormal_data] = self.take_subset_of_feats(normal_data, abnormal_data)
        logging.info("\tRead features with shape {}, {}.".format(normal_data.shape, abnormal_data.shape))

        normal_train_fold_ids, normal_test_fold_ids = self.train_test_split(len(normal_data))
        abnormal_train_fold_ids, abnormal_test_fold_ids = self.train_test_split(len(abnormal_data))

        self.cross_validate(normal_data, normal_train_fold_ids, normal_test_fold_ids,
                            abnormal_data, abnormal_train_fold_ids, abnormal_test_fold_ids)

# ______________________________________________________________________________________________________________________
    def get_results(self):
        return self.results

# ______________________________________________________________________________________________________________________
    def __init__(self, n_folds=10, n_proc=1):
        self.n_folds = n_folds
        self.n_proc = n_proc
        self.scaler = None

        # 4 metrics: accuracy, precision, recall, f1
        self.results = np.ndarray(shape=(self.n_folds, 4), dtype=float)
